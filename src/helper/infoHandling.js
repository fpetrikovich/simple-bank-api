
const numDaysBetween = require('./dateUtils');

// Limit the balance history to the last specified days
function limitBalanceHistory(account, days) {
    const today = new Date();
    let response = [];
    let idx = 0;

    // Sorting by descending date 
    account.balanceHistory.sort((a, b) => b.date - a.date)

    // Add to response while the difference between the dates is less or equal to the specified days
    while (idx < account.balanceHistory.length && numDaysBetween(today, account.balanceHistory[idx].date) <= days) {
        response.push(account.balanceHistory[idx++])
    }
    return response;
}

exports.retrieveBasicUserDetails = function(account) {
    return {
        firstName: account.personalData.firstName,
        lastName: account.personalData.lastName,
        dni: account.personalData.dni,
        cbu: account.cbu
    };
}

// Send only none sensitive information
exports.ownerAccountDetails = function(account) {
    return {
        personalData: {
            firstName: account.personalData.firstName,
            lastName: account.personalData.lastName,
            dni: account.personalData.dni,
            email: account.personalData.email,
        },
        balance: account.balance,
        balanceHistory: limitBalanceHistory(account, 5),
        cbu: account.cbu,
    }
}
