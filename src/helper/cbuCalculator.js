const CONSTANTS = require('../constants');

function calculateAccountCode() {
    const codeLength = 13;
    const currentDate = new Date();
    return currentDate.getTime().toString(CONSTANTS.BASE).substring(0, codeLength);
}

function calculateFirstVerificationDigit(bankCode, branchCode) {
    // Pass the numbers to digits in an array
    const bankCodeArr = [
        parseInt(bankCode.charAt(0)), 
        parseInt(bankCode.charAt(1)), 
        parseInt(bankCode.charAt(2))
    ];
    const bankBranchArr = [
        parseInt(branchCode.charAt(0)), 
        parseInt(branchCode.charAt(1)), 
        parseInt(branchCode.charAt(2)),
        parseInt(branchCode.charAt(3))
    ];

    // SOURCE -> https://es.wikipedia.org/wiki/Clave_Bancaria_Uniforme
    let sum = bankCodeArr[0]*7 + bankCodeArr[1] + bankCodeArr[2]*3;
    sum += bankBranchArr[0]*9 + bankBranchArr[1]*7 + bankBranchArr[2] + bankBranchArr[3]*3;
    
    // Subtract the last digit of sum
    let difference = 10 - sum%10;

    // Return difference and 0 if difference is 10
    return difference < 10 ? difference.toString(CONSTANTS.BASE) : "0";
}

function calculateSecondVerificationDigit(accountCode) {
    // Pass the numbers to digits in an array
    const accountCodeArr = [
        parseInt(accountCode.charAt(0)), 
        parseInt(accountCode.charAt(1)), 
        parseInt(accountCode.charAt(2)),
        parseInt(accountCode.charAt(3)), 
        parseInt(accountCode.charAt(4)), 
        parseInt(accountCode.charAt(5)), 
        parseInt(accountCode.charAt(6)), 
        parseInt(accountCode.charAt(7)), 
        parseInt(accountCode.charAt(8)), 
        parseInt(accountCode.charAt(9)), 
        parseInt(accountCode.charAt(10)), 
        parseInt(accountCode.charAt(11)), 
        parseInt(accountCode.charAt(12)), 
    ];

    const multiplier = [3, 9, 7, 1, 3, 9, 7, 1, 3, 9, 7, 1, 3]

    // SOURCE -> https://es.wikipedia.org/wiki/Clave_Bancaria_Uniforme
    let sum = 0;
    for (i = 0; i < accountCodeArr.length; i++) {
        sum += accountCodeArr[i] * multiplier[i];
    }

    // Subtract the last digit of sum
    let difference = 10 - sum%10;

    // Return difference and 0 if difference is 10
    return difference < 10 ? difference.toString(CONSTANTS.BASE) : "0";
}

function calculateCBU() {
    // Calculate the 1st verification digit using bank and branch code
    const firstVerificationDigit = calculateFirstVerificationDigit(CONSTANTS.BANK_CODE, CONSTANTS.BRANCH_CODE);
    // Generate random 13 digit code for account
    const accountCode = calculateAccountCode();
    // Calculate the 2nd verification digit using bank and branch code
    const secondVerificationDigit = calculateSecondVerificationDigit(accountCode);

    return CONSTANTS.BANK_CODE + CONSTANTS.BRANCH_CODE + firstVerificationDigit + accountCode + secondVerificationDigit;
}

module.exports = calculateCBU;