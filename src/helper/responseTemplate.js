
function genericResponse(res, status, code, message) {
    return res.status(code).send({
        status: status,
        message: message,
    });
}

module.exports = genericResponse;