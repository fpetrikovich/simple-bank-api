

const Account = require('../accounts/accountModel');
const cbuCalulator = require('../helper/cbuCalculator');

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config');

// Handle registration actions
exports.register = (req, res) => {
    if (!(req.body.firstName && req.body.lastName && req.body.dni && req.body.email && req.body.password && req.body.birthdate)) {
        return res.status(404).send('Missing details. Must include first and last name, DNI, email, password, and birthdate');
    }
    // Encrypting password and security PIN
    const hashedPassword = bcrypt.hashSync(req.body.password, 8);
    const hashedPIN = req.body.pin ? bcrypt.hashSync(req.body.pin, 8) : null;

    const accountInfo = {
        personalData: {
            firstName:  req.body.firstName,
            lastName: req.body.lastName, 
            dni: req.body.dni,
            email: req.body.email,
            password: hashedPassword,
            birthdate: req.body.birthdate,
        },
        balance: 0,
        balanceHistory: [{balance: 0, date: new Date()}], 
        securityPIN: hashedPIN,
        cbu: cbuCalulator(),
    };

    Account.create(accountInfo, (err, account) => {
        if (err) return res.status(500).send("There was a problem registering the account.\nError: " + err)
        // Create a token with the account _id
        const token = jwt.sign({ id: account._id }, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        res.status(201).send({ auth: true, token: token, cbu: account.cbu });
    })
};

exports.login = (req, res) => {
    if (!req.body.email || !req.body.password) {
        return res.status(404).send('Missing details. Must include email and password');
    }
    // Checks if the email exists
    Account.findOne({ "personalData.email": req.body.email }, {"personalData.$": 1}, (err, account) => {
        if (err) return res.status(500).send('Error on the server.');
        if (!account) return res.status(404).send('No user found.');
        
        // Check if the password hashes match, if not then 401 -> Unauthorized
        const passwordIsValid = bcrypt.compareSync(req.body.password, account.personalData.password);
        if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });
        
        // Create the token which lasts for 24 hours
        const token = jwt.sign({ id: account._id }, config.secret, {
          expiresIn: 86400 // expires in 24 hours
        });
        // Login successful
        res.status(200).send({ auth: true, token: token });
    });
}

// When logging out, the token is destroyed on the client
// This endpoint is created to logically depict what happens when you log out
exports.logout = (req, res) => {
    res.status(200).send({ auth: false, token: null })
}
