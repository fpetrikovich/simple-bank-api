
const jwt = require('jsonwebtoken');
const config = require('../config');

// Middleware function to check the validity of tokens
function verifyToken(req, res, next) {
  const token = req.headers['x-access-token'];

  // If no token, then 403 -> FORBIDDEN
  if (!token)
    return res.status(403).send({ auth: false, message: 'No token provided.' });
    
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
        return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
    }
    // if everything good, save to request for use in other routes
    req.accountId = decoded.id;
    // Flow continues to the next function
    next();
  });
}

module.exports = verifyToken;