// accountController.js

const Account = require('./accountModel');
const bcrypt = require('bcryptjs');
const infoHandling = require('../helper/infoHandling');
const genericResponse = require('../helper/responseTemplate');
const isValidSecurityPin = require('../validations/securityPinValidator')

// Constants
const ERROR = require('../constants').STATUS_ERROR;
const SUCCESS = require('../constants').STATUS_SUCCESS;

// Handle index actions
exports.index = (req, res) => {
    Account.get((err, accounts) => {
        if (err) {
            genericResponse(res, ERROR, 500, "There was a problem listing the account.");
        } else {
            // Only show the basic information when listing accounts 
            const basicAccountInfo = accounts.map(account => infoHandling.retrieveBasicUserDetails(account));
            res.status(200).send({
                accounts : basicAccountInfo
            });
        }
    });
};

// Handle view contact info
exports.view = (req, res) => {
    Account.findOne({ cbu: req.params.cbu }, (err, account) => {
        if (err) {
            genericResponse(res, ERROR, 500, "There was a problem finding the account.");
        }
        // Not found -> none existing CBU
        if (!account) {
            return genericResponse(res, ERROR, 404, "Specified CBU does not exists");
        } 
        res.status(200).send(
            infoHandling.retrieveBasicUserDetails(account)
        );       
    });
};


exports.me = (req, res) => {
    Account.findById(req.accountId, (err, account) => {
        if (err) return genericResponse(res, ERROR, 500, "There was a problem finding the account.");
        if (!account) return genericResponse(res, ERROR, 500, "No account found.");
        
        res.status(200).send(infoHandling.ownerAccountDetails(account));
    });
}

// Handle update contact info
// ASSUMING the only thing that can be updated is the security PIN for simplicity */
exports.updatePin = (req, res) => {
    if (!req.body.pin) return genericResponse(res, ERROR, 400, "Must specify a security PIN to update it")

    Account.findById(req.accountId, (err, account) => {
        if (err) return genericResponse(res, ERROR, 500, "There was a problem finding the account.");
        if (!account) return genericResponse(res, ERROR, 404, "No account found.");
        
        if (isValidSecurityPin(req.body.pin)) {
            account.securityPIN = bcrypt.hashSync(req.body.pin, 8),
            // save the account and check for errors
            account.save((err) => {
                if (err) {
                    return genericResponse(res, ERROR, 500, err)
                } 
                return genericResponse(res, SUCCESS, 200, "Account Security PIN updated")
            });
        } else {
            return genericResponse(res, ERROR, 500, "Invalid PIN")  
        }
    });
}

// Handle delete contact
exports.delete = (req, res) => {
    Account.findByIdAndDelete(req.accountId, (err, info) => {
        if (err) {
            return genericResponse(res, ERROR, 500, "There was a problem deleting the account")
        }
        res.status(204).send({});
    });
};