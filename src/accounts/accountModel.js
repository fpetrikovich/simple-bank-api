const mongoose = require('mongoose');
const validator = require('validator');

const accountSchema = new mongoose.Schema({
    // _id: {type: String},
    personalData: {
        firstName:  {
            type: String,
            required: true,
        },
        lastName: {
            type: String,
            required: true,
        },
        dni: {
            type: Number,
            required: true,
            unique: true, 
        },
        email: {
            type: String,
            required: true,
            unique: true,
            validate: (value) => {
                return validator.isEmail(value)
            }
        },
        password: {
            type: String, 
            required: true, 
        },
        birthdate:  {
            type: Date,
            required: true,
        },
    },  
    securityPIN: {
        type: String,
        required: false,
    },
    balance: {
        type: Number,
        required: true,
    },
    balanceHistory: {
        type: Array,
        required: true,
    },
    cbu: {
        type: String,
        maxlength: 22,
        minlength: 22,
        unique: true,
    }
})

// Export Account model
const Account = module.exports = mongoose.model('account', accountSchema);

module.exports.get = function (callback, limit) {
    Account.find(callback).limit(limit);
}