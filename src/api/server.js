// server.js

// Imports
let express = require('express')
let connectDb = require("./connection");
let apiRoutes = require("./apiRoutes")

// Initialize the app
let app = express();

// Use Api routes in the App
app.use('/api', apiRoutes)

// Connect to the database
connectDb()
    .then(() => console.log("Database connection successful"))
    .catch(err => console.error("Database connection error.", err));

// Setup server port
const port = process.env.PORT || 8080;

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express and Nodemon'));
// Launch app to listen to specified port
app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});