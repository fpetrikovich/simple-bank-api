// apiRoutes.js
// Initialize express router
const router = require('express').Router();

// Middleware function for token validity
const verifyToken = require('../auth/verifyToken')

const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API is working',
        message: 'Welcome to the bank\'s API!'
    });
});

// Import controllers
const accountController = require('../accounts/accountController');
const transactionController = require('../transactions/transactionController');
const authController = require('../auth/authController');

// Routes Auth
router.route('/register')
    .post(authController.register);
router.route('/login')
    .get(authController.login);
router.route('/logout')
    .get(authController.logout);

// Routes Accounts
router.route('/accounts')
    .get(accountController.index);
router.route('/accounts/:cbu')
    .get(accountController.view)

// Routes Transactions
router.route('/transactions/deposit')
    .put(transactionController.deposit)
router.route('/transactions')
    .get(transactionController.index)
    .post(verifyToken, transactionController.create)
router.route('/transactions/confirm')
    .put(verifyToken, transactionController.confirm)
router.route('/transactions/:id')
    .get(transactionController.view)

// Routes with token required
router.route('/me')
    .get(verifyToken, accountController.me)
    .delete(verifyToken, accountController.delete);
router.route('/pin')
    .patch(verifyToken, accountController.updatePin)
    .put(verifyToken, accountController.updatePin);

// Export API routes
module.exports = router;