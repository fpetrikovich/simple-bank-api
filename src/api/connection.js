// connection.js
const mongoose = require('mongoose');

const server = 'localhost:27017'
const database = 'bankdb';
const connection = `mongodb://${server}/${database}?replicaSet=mongo-repl-set`;

function connectDb() {
    return mongoose.connect(connection, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
};
module.exports = connectDb;