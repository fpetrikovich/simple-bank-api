// Requires
const db = require('mongoose');
const Transaction = require('./transactionModel');
const Account = require('../accounts/accountModel');
const genericResponse = require('../helper/responseTemplate');
const bcrypt = require('bcryptjs');

// Constants
const CONSTANTS = require('../constants');
const ERROR = CONSTANTS.STATUS_ERROR;
const SUCCESS = CONSTANTS.STATUS_SUCCESS;
const MAX_TRANSFER_AMOUNT = CONSTANTS.MAX_TRANSFER_AMOUNT;
const TRANSACTIONS_PER_PAGE = CONSTANTS.TRANSACTIONS_PER_PAGE;

async function updateAccountBalance(account, amount, date) {
    account.balance += amount;
    account.balanceHistory.push({
        balance: account.balance,
        date: date
    });
    // Return the promise
    return account.save();
}

// Returns a promise 
async function updateBalancesTransaction(sender, recipient, amount, date) {
    return Promise.all([
        updateAccountBalance(sender, -1 * amount, date), 
        updateAccountBalance(recipient, amount, date)
    ]);
}

// Commit transaction since all went well and send response back
async function commitTransactionWithResponse(session, res, msg) {
    await session.commitTransaction();
    session.endSession();
    res.status(201).send(msg);
}

// Rollback of transaction
async function abortTransactionWithResponse(session, res, code, err) {
    await session.abortTransaction();
    session.endSession()
    genericResponse(res, ERROR, code, "There was a problem during the transaction. Error: " + err); //rollback             
}

// Find transaction by id
exports.view = (req, res) => {
    Transaction.findById(req.params.id, (err, moneyTransaction) => {
        if (err) return res.status(500).send("There was a problem finding the transaction.");
        if (!moneyTransaction) return res.status(404).send("No transaction found.");
        
        res.status(200).send(moneyTransaction);
    });
}

// Handle deposit actions
exports.deposit = (req, res) => {
    if (!req.body.amount || !req.body.cbu) {
        return genericResponse(res, ERROR, 400, "Must specify amount and CBU for the deposit");
    }
    Account.findOne({ cbu: req.body.cbu }, (err, account) => {
        if (err) {
            return genericResponse(res, ERROR, 500, "There was a problem finding the account.");
        }
        // Not found -> none existing CBU
        if (!account) {
            return genericResponse(res, ERROR, 404, "Specified CBU does not exists");
        } 

        // Updating the recieving account's balance
        updateAccountBalance(account, parseInt(req.body.amount), new Date())
            .then(() => genericResponse(res, SUCCESS, 200, `Account ${account.cbu} balance updated`))
            .catch((err) => genericResponse(res, ERROR, 500, "Balance update unsuccessful"));
    });
};

exports.index = async (req, res) => {
    const page = req.query.page || 1;
    let dateRange = { }
    let lowerLimit = undefined, upperLimit = undefined;

    if (req.query.lowerLimit) {
        lowerLimit = new Date(req.query.lowerLimit)
        dateRange["$gte"] = lowerLimit;
    }
    if (req.query.upperLimit) {
        upperLimit = new Date(req.query.upperLimit)
        upperLimit.setUTCHours(23,59,59,999);
        dateRange["$lte"] = upperLimit;
    }
    // Invalid range -> BAD REQUEST
    if (lowerLimit && upperLimit && lowerLimit > upperLimit) {
        return genericResponse(res, ERROR, 400, "Invalid date range.")
    }
    // Set the valid range query
    const query = !lowerLimit && !upperLimit ? {} : {date: dateRange};

    try {
        const transactionResults = await Transaction.find(query)
            .skip(TRANSACTIONS_PER_PAGE * (page - 1))
            .limit(TRANSACTIONS_PER_PAGE)
            .sort('-date');

        res.status(200).send({
            transactionsResults: transactionResults
        });
    } catch (err) {
        genericResponse(res, ERROR, 500, "There was a problem listing the transactions.");
        return;
    }
}

// Create a transaction, requires a token
exports.create = async (req, res) => {
    // Check if all required values were sent
    if (!(req.body.amount && req.body.recipient)) {
        return genericResponse(res, ERROR, 400, "Must specify recipient and amount to create a transaction");
    }

    // Want this method to be transactional
    const session = await db.startSession();
    session.startTransaction();

    try {
        // SENDER LOGIC
        const sender = await Account.findById(req.accountId).session(session);
        // Retrieved the sender with the token
        if (!sender) return genericResponse(res, ERROR, 404, "No account found for specified token.");
        if (sender.balance < parseInt(req.body.amount)) return genericResponse(res, ERROR, 404, `Insuficient funds in account ${sender.cbu}.`);
        
        // RECIPIENT LOGIC
        if (!req.body.recipient) return genericResponse(res, ERROR, 404, "Missing recipient CBU or transfer amount.");
        const recipient = await Account.findOne({cbu : req.body.recipient}).session(session);

        if (!recipient) return genericResponse(res, ERROR, 404, "Recipient's account not found.");

        // Cannot send money to your own account
        if (sender.cbu === recipient.cbu) return abortTransactionWithResponse(session, res, 400, "Can not transfer money to the same account as the emitting one.");

        // Setting the information for the transaction
        const now = new Date();
        const transactionInfo = {
            sender: sender.cbu,
            recipient: recipient.cbu,
            amount: parseInt(req.body.amount),
            date: now,
            confirmed: parseInt(req.body.amount) <= MAX_TRANSFER_AMOUNT,
        }

        // MONEY TRANSACTION LOGIC
        const moneyTransaction = (await Transaction.create([transactionInfo], {session: session}))[0];
        // Update the balances only if the transaction is confirmed 
        if (moneyTransaction.confirmed) {
            updateBalancesTransaction(sender, recipient, moneyTransaction.amount, now)
                .then(async () => commitTransactionWithResponse(session, res, { transactionId: moneyTransaction._id}))
                .catch(async (err) => abortTransactionWithResponse(session, 500, res, err));  //rollback            
        } else {
            commitTransactionWithResponse(session, res, { transactionId: moneyTransaction._id})
        }     
    } catch(err) {
        abortTransactionWithResponse(session, res, 500, err)
    }
}

// Confirms an existing transaction, updating the balance of those involved
// Token of the sender required
exports.confirm = async (req, res) => {
    if (!(req.body.transactionId && req.body.pin)) {
        return genericResponse(res, ERROR, 400, "Must specify transaction id and security PIN");
    }
    // Want this method to be transactional
    const session = await db.startSession();
    session.startTransaction();

    // Success message
    try {
        const moneyTransaction = await Transaction.findById(req.body.transactionId).session(session)
        const resMsg = {status: SUCCESS, message: `Transaction ${moneyTransaction._id} confirmed.`}
        if (!moneyTransaction) {
            return abortTransactionWithResponse(session, res, 404, "No transaction found.");
        }

        // Transaction must be pending to confirm
        if (!moneyTransaction.confirmed) {
            // Sender must specify token and match the transaction sender
            const loggedUser = await Account.findById(req.accountId).session(session);
            if (!loggedUser) return abortTransactionWithResponse(session, res, 500, "Sender no longer exists");
            if (loggedUser.cbu !== moneyTransaction.sender) return abortTransactionWithResponse(session, res, 401, "Unauthorized access");
            if (!loggedUser.securityPIN) return abortTransactionWithResponse(session, res, 401, `Must create a security pin to transfer more than ${CONSTANTS.MAX_TRANSFER_AMOUNT} pesos`);

            // Obtain recipient
            const recipient = await Account.findOne({cbu : moneyTransaction.recipient}).session(session);
            if (!recipient) return abortTransactionWithResponse(session, res, 500, "Recipient no longer exists");

            // Check PIN validity
            const pinIsValid = bcrypt.compareSync(req.body.pin, loggedUser.securityPIN);
            if (pinIsValid) {
                // Update balances and confirm transaction
                await updateBalancesTransaction(loggedUser, recipient, moneyTransaction.amount, moneyTransaction.date);
                moneyTransaction.confirmed = true;
                await moneyTransaction.save();
            } else {
                throw 'Invalid security PIN entered'
            }
        }
        commitTransactionWithResponse(session, res, resMsg)
    } catch (err) {
        abortTransactionWithResponse(session, res, 500, err) // Rollback
    }
 
 }