const mongoose = require('mongoose');
const validator = require('validator');

const transactionSchema = new mongoose.Schema({
    sender: {
        type: String,
        required: true,
    },
    recipient: {
        type: String,
        required: true, 
    },
    amount: {
        type: Number,
        required: true,
    },
    date: {
        type: Date,
        required: true,
    },
    confirmed: {
        type: Boolean, 
        required: true
    }
})
const Transaction = module.exports = mongoose.model('transaction', transactionSchema);

module.exports.get = function (callback, limit) {
    Transaction.find(callback).limit(limit);
}
