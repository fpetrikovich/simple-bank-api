const PIN_REQUIRED_LENGTH = require('../constants').PIN_REQUIRED_LENGTH;

// PIN must be 8 digits long
function isValidSecurityPin(pin) {
    return pin.toString().length === PIN_REQUIRED_LENGTH;
}

module.exports = isValidSecurityPin;