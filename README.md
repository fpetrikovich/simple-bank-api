# Corriendo la API Bancaria
API REST que permita replicar el funcionamiento de ciertas verticales bancarias. La idea es que cada cliente pueda operar su cuenta bancaria sin necesidad de un frontend y de esta manera innovar frente al resto de las entidades financieras.

## Corriendo MongoDB Replica Set en Docker
Para usar MongoDB, vamos a utilizar un contenedor de Docker.

Primero hay que pullear la imagen:
```
sudo docker pull mongo
```
imple Bank 
Creamos una red de docker:
```
docker network create mongo-cluster
```

Creamos los contenedores mongo que deseamos correr en la red creada. En este caso solo se utilizo un contenedor.
```
docker run -d --net mongo-cluster -p 27017:27017 --name <CONTAINER_NAME> mongo mongod --replSet mongo-repl-set --port 27017
```

Entramos al contenedor creado:
```
docker exec -it <CONTAINER_NAME> mongo
```

Una vez dentro, configuramos el replica set:
```
db = (new Mongo('localhost:27017')).getDB('bankdb')
config={
    "_id":"mongo-repl-set",
    "members":[{"_id":0,"host":"localhost:27017"}]
}
rs.initiate(config)
```
Donde bankdb es el nombre de la base de datos que se usara.
La respuesta deberia ser parecida a lo siguiente:
```
{
	"ok" : 1,
	"$clusterTime" : {
		"clusterTime" : Timestamp(1567674525, 1),
		"signature" : {
			"hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
			"keyId" : NumberLong(0)
		}
	},
	"operationTime" : Timestamp(1567674525, 1)
}
```

## Corriendo la API
Para este proyecto, se utilizo Node JS. Para instalarlo en Ubuntu:
```
sudo apt install nodejs
```

Luego de clonarse el repo y pararse sobre el root del proyecto, correr:
```
node scr/api/server
```
La API estara corriendo correctamente si se imprime:
```
Server is listening on port <PORT>
Database connection successful
```

# REST API Documentación
En esta sección se incluiran comandos cURL de ejemplo que llaman a cada endpoint de la base.
## Autenticación
### Registración
Las cuentas bancarias se pueden registrar con el endpoing /api/register. 
El pin de seguridad de la cuenta es opcional durante la creación, en el siguiente ejemplo se incluye.
```
curl --location --request POST -i 'http://localhost:8080/api/register' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'firstName=Arya' \
--data-urlencode 'lastName=Stark' \
--data-urlencode 'dni=40222820' \
--data-urlencode 'email=aryastark@gmail.com' \
--data-urlencode 'password=password' \
--data-urlencode 'birthdate=2001/12/12' \
--data-urlencode 'pin=12345678'
```
El endpoint devuelve un token de autenticación al igual que el CBU de la cuenta.

### Login
Una vez creado un usuario, existe un endpoint de login que devuelve el token de autenticación si el email y password ingresado son correctos.
```
curl --location --request GET -i 'http://localhost:8080/api/login' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'email=eddardstartk@gmail.com' \
--data-urlencode 'password=password'
```

## Cuentas Bancarias
### Listar cuentas existentes
Existe un endpoint para listar las cuentas existente, donde solo se pone la información básica del dueño de la misma y el CBU.
```
curl --location --request GET -i 'http://localhost:8080/api/accounts'
```

### Datos de una cuenta
Se puede obtener la información básica de un usuario teniendo su CBU.
```
curl --location --request GET -i 'http://localhost:8080/api/accounts/<CBU>'
```

### Datos de mi cuenta
Si se desean obtener datos más especificos de la cuenta, como información del usuario, CBU, su balance y su historial de balance de los ultimos 5 dias, se debe especificar el token de autenticación de la cuenta en los headers.
```
curl --location --request GET -i 'http://localhost:8080/api/me' \
--header 'x-access-token: <TOKEN>'
```

### Cambiar el PIN de seguridad
Para transferencias bancarias de más de 10.000 pesos, un PIN de seguridad de 8 digitos es necesario para confirmar la transferencia. El siguiente endpoint crea en caso de no existir o altera el existente. Se debe especificar el token de autenticación de la cuenta en los headers.
```
curl --location --request PUT -i 'http://localhost:8080/api/pin' \
--header 'x-access-token: <TOKEN>' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'pin=00000000'
```

### Borrado de una cuenta
Se puede borrar la cuenta teniendo el token de autenticación de la misma.
```
curl --location --request DELETE -i 'http://localhost:8080/api/me' \
--header 'x-access-token: <TOKEN>'
```

## Transferencias Bancarias
### Depósito
Con este endpoint se simula un depósito a una cuenta. Se debe especificar el CBU de la misma y el monto. Generado con motivos de testin.
```
curl --location --request PUT 'http://localhost:8080/api/transactions/deposit' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'cbu=<CBU_DEL_DESTINATARIO>' \
--data-urlencode 'amount=<MONTO>'
```

### Creación
Para generar una transferencia, se requiere establece el token de autenticación del remitente en los headers y el monto y el CBU del destinatario en el cuerpo de solicitud.
```
curl --location --request POST -i 'http://localhost:8080/api/transactions' \
--header 'x-access-token: <TOKEN_DEL_REMITENTE>' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'amount=<MONTO>' \
--data-urlencode 'recipient=<CBU_DEL_DESTINATARIO>'
```
El endpoint devuelve el id de la transferencia.

### Confirmación
Para transferencias con monto mayores a 10.000 pesos, se requiere ingresar el PIN de seguridad. Se envia el token del remitente en los headers y el id de la transferencia y el PIN en el cuerpo de la solicitud. La transferencia debe estar creada previamente con el endpoint anterior.
```
curl --location --request PUT -i 'http://localhost:8080/api/transactions/confirm' \
--header 'x-access-token: <TOKEN_DEL_REMITENTE> \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'transactionId=<ID_DE_TRANSFERENCIA>' \
--data-urlencode 'pin=<PIN_DE_8_DIGITOS>'
```

### Obtener transferencia por id
Especificando el id de la transferencia, el cual es devuelto en su creacion, se puede obtener los datos de la misma con el siguiente cURL:
```
curl --location --request GET -i 'http://localhost:8080/api/transactions/<ID_TRANSFERENCIA>'
```
En la respuesta, confirmed identifica si los montos de las cuentas fueron actualizadas, dado que transferencias de más de 10.000 pesos necesitan confirmación.

### Obtener historial de transferencias
Lista de transferencias indicando rangos temporales. Los resultados que se obtiene estan paginados, pudiendo por tanto, indicar en el request el número de página solicitada. Ejemplo de uso:
```
curl --location --request GET 'http://localhost:8080/api/transactions?lowerLimit=2021-01-17&upperLimit=2021-01-18&page=2'
```